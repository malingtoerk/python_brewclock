# Brew Clock #

A simple Python (v3.8) script to basically output the difference between when a brew is set to ferment and the current time. Intended just to be used for displaying at https://dlive.tv/MalingToerk using OBS streaming software. But if anyone else happens to find it useful or educational feel free.

### How do I get set up? ###

* Simply download and execute the 'brewclock' python script..
* **-h / --help** shows typical help text..
* **-i / --input** is the timestamp file to calculate from.
* **-o / --output** is the file where the time difference gets written to.
* **-r / --reset** sets the input timestamp file to current time.

### Advice.. ###

* Since the output file is written to quite frequently, it might be a good idea to use a ramdisk..

### Who do I talk to? ###

* See sourcecode..